import matplotlib.pyplot as plt
from typing import Dict, List

# State abbreviations stolen from https://gist.github.com/rogerallen/1583593
def abbreviate_state(state_name: str):
    abbrevs = {
    'alabama': 'AL',
    'alaska': 'AK',
    'arizona': 'AZ',
    'arkansas': 'AR',
    'california': 'CA',
    'colorado': 'CO',
    'connecticut': 'CT',
    'delaware': 'DE',
    'district of columbia': 'DC',
    'florida': 'FL',
    'georgia': 'GA',
    'guam': 'GU',
    'hawaii': 'HI',
    'idaho': 'ID',
    'illinois': 'IL',
    'indiana': 'IN',
    'iowa': 'IA',
    'kansas': 'KS',
    'kentucky': 'KY',
    'louisiana': 'LA',
    'maine': 'ME',
    'maryland': 'MD',
    'massachusetts': 'MA',
    'michigan': 'MI',
    'minnesota': 'MN',
    'mississippi': 'MS',
    'missouri': 'MO',
    'montana': 'MT',
    'nebraska': 'NE',
    'nevada': 'NV',
    'new hampshire': 'NH',
    'new jersey': 'NJ',
    'new mexico': 'NM',
    'new york': 'NY',
    'new york city': 'NYC',
    'north carolina': 'NC',
    'north dakota': 'ND',
    'ohio': 'OH',
    'oklahoma': 'OK',
    'oregon': 'OR',
    'pennsylvania': 'PA',
    'puerto rico': 'PR',
    'rhode island': 'RI',
    'south carolina': 'SC',
    'south dakota': 'SD',
    'tennessee': 'TN',
    'texas': 'TX',
    'utah': 'UT',
    'vermont': 'VT',
    'virgin islands': 'VI',
    'virginia': 'VA',
    'washington': 'WA',
    'west virginia': 'WV',
    'wisconsin': 'WI',
    'wyoming': 'WY',
    'united states': 'USA'
    }

    return abbrevs[state_name.lower()]


class WeekStat(object):
    def __init__(self, year: int, week: int, deaths: int):
        self.year = year
        self.week = week
        self.deaths = deaths

    def __eq__(self, other):
        """ Compare just the dates """
        return self.year == other.year and self.week == other.week

    def sort_week(self):
        year_str = '{0:04}'.format(self.year)
        week_str = '{0:02}'.format(self.week)
        return f'{year_str}-{week_str}'

    def __str__(self):
        return f'year: {self.year}, week: {self.week}, deaths: {self.deaths}'


class StateInfo(object):
    def __init__(self, state_name: str):
        self.state_name = state_name
        self.month_stats = []  # type: List[WeekStat]
        self.deaths_by_year = {}  # type: Dict[int, int]
        
    def add_deaths(self, year: int, week: int, deaths: int) -> None:
        month_stat = WeekStat(year, week, deaths)
        if month_stat in self.month_stats:
            raise Exception(f'Already added year-week {year}-{week} for state {self.state_name}.')
        self.month_stats.append(month_stat)
        if year not in self.deaths_by_year:
            self.deaths_by_year[year] = 0
        self.deaths_by_year[year] += deaths


class SeasonalityGraphMaker(object):
    def __init__(self):
        self.input_file_paths = [
            'Weekly_Counts_of_Deaths_by_State_and_Select_Causes__2019-2020.csv',
            'Weekly_Counts_of_Deaths_by_State_and_Select_Causes__2014-2018.csv']
        self.state_infos = {}  # type: Dict[str, StateInfo]

    def load_data(self) -> None:
        for input_file_path in self.input_file_paths:
            with open(input_file_path, 'r') as in_file:
                for line in in_file.readlines():
                    fields = line.split(',')
                    state_name, year_str, week_num_str, date_str, num_deaths_all_cause_str = fields[0:5]
                    if year_str == 'MMWR Year':
                        continue
                    if num_deaths_all_cause_str == '':
                        print(f'Missing num_deaths_all_cause field for 1 row, {state_name} {year_str}.')
                        continue
                    year = int(year_str)
                    week_num = int(week_num_str)
                    num_deaths_all_cause = int(num_deaths_all_cause_str)

                    if state_name not in self.state_infos:
                        self.state_infos[state_name] = StateInfo(state_name)
                    self.state_infos[state_name].add_deaths(year, week_num, num_deaths_all_cause)

    def write_all_state_plots(self) -> None:
        for state_name in self.state_infos.keys():
            print(f'Writing for state {state_name}...')
            self.write_plot_one_state(state_name)

    def write_plot_one_state(self, state_name) -> None:
        one_state = self.state_infos[state_name]
        weeks_by_year = {}
        values_by_year = {}
        for ms in sorted(one_state.month_stats, key=lambda month_stat: month_stat.sort_week()):
            if ms.year not in weeks_by_year:
                weeks_by_year[ms.year] = []
                values_by_year[ms.year] = []
            values_by_year[ms.year].append(ms.deaths)
            weeks_by_year[ms.year].append(ms.week)
        data = zip(weeks_by_year.values(), values_by_year.values())
        colors = ('pink', 'gray', 'green', 'purple', 'blue', 'black', 'red')
        state_abbrev = abbreviate_state(one_state.state_name)
        groups = [f'{state_abbrev} {year}' for year in weeks_by_year.keys()]
        figure = plt.figure()
        ax = figure.add_subplot(1, 1, 1)
        for data, color, group in zip(data, colors, groups):
            x, y = data
            #ax.scatter(x, y, alpha=0.8, c=color, edgecolors='none', s=10, label=group)
            ax.plot(x, y, alpha=0.8, c=color, label=group)
        plt.title(f'{state_name} deaths by week')
        plt.xlabel('week #')
        plt.ylabel('deaths in week')
        plt.legend(loc=3, fontsize=6)
        plt.savefig(f'{state_name}.png')
        plt.close()


if __name__ == '__main__':
    app = SeasonalityGraphMaker()
    app.load_data()
    app.write_all_state_plots()
